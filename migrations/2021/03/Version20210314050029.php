<?php

declare(strict_types=1);

namespace Migrations;

use Doctrine\DBAL\Schema\Schema;
use Doctrine\Migrations\AbstractMigration;

/**
 * Auto-generated Migration: Please modify to your needs!
 */
final class Version20210314050029 extends AbstractMigration
{
    public function getDescription() : string
    {
        return '';
    }

    public function up(Schema $schema) : void
    {
        $translations = [
            ['original' => 'feedback.overview', 'hash' => 'efb192e671bc4bbc88d44a96c62aa3c0', 'module' => 'admin', 'language_id' => 1, 'singular' => 'Zpětná vazba', 'plural1' => '', 'plural2' => ''],
            ['original' => 'role-resource.feedback.title', 'hash' => '3bc2ddfaf535fbff815ddc51acd36566', 'module' => 'admin', 'language_id' => 1, 'singular' => 'Zpětná vazba', 'plural1' => '', 'plural2' => ''],
            ['original' => 'role-resource.feedback.description', 'hash' => '7c320fef51f060e706fa1adf92e9f58d', 'module' => 'admin', 'language_id' => 1, 'singular' => 'umožňuje spravovat zpětné vazby', 'plural1' => '', 'plural2' => ''],
            ['original' => 'feedback.overview.title', 'hash' => 'b681a8774bdaba8df62ad9dc355a4354', 'module' => 'admin', 'language_id' => 1, 'singular' => 'Zpětná vazba|Přehled', 'plural1' => '', 'plural2' => ''],
            ['original' => 'grid.feedback.overview.status.unarchive', 'hash' => '5145d1c51bd39e6b7a986060b3575e7b', 'module' => 'admin', 'language_id' => 1, 'singular' => 'ne', 'plural1' => '', 'plural2' => ''],
            ['original' => 'grid.feedback.overview.status.archive', 'hash' => '3724e4e2add1ded1def0c6cf00487069', 'module' => 'admin', 'language_id' => 1, 'singular' => 'ano', 'plural1' => '', 'plural2' => ''],
            ['original' => 'grid.feedback.overview.is-active', 'hash' => 'ff13a0fff3a47c495602ee409145a952', 'module' => 'admin', 'language_id' => 1, 'singular' => 'Je archivovaná?', 'plural1' => '', 'plural2' => ''],
            ['original' => 'grid.feedback.overview.name', 'hash' => '9e608d20d5646f01a69fa290a4e81f4e', 'module' => 'admin', 'language_id' => 1, 'singular' => 'Název', 'plural1' => '', 'plural2' => ''],
            ['original' => 'grid.feedback.overview.static-name', 'hash' => 'ff82d81b1ae90f648bc03fb54afa29d7', 'module' => 'admin', 'language_id' => 1, 'singular' => 'Autor', 'plural1' => '', 'plural2' => ''],
            ['original' => 'grid.feedback.overview.created-at', 'hash' => '63cf5cd49a6494b7ea52fbbd6513ca8d', 'module' => 'admin', 'language_id' => 1, 'singular' => 'Vytvořeno', 'plural1' => '', 'plural2' => ''],
            ['original' => 'grid.feedback.overview.action.archive', 'hash' => '7627414f2a2e9c41b50ab88ed6c9d120', 'module' => 'admin', 'language_id' => 1, 'singular' => '', 'plural1' => '', 'plural2' => ''],
            ['original' => 'grid.feedback.overview.action.archive.title', 'hash' => 'f4a46d7ba51d32cea7f0b5e03e671d06', 'module' => 'admin', 'language_id' => 1, 'singular' => 'přesunout do archívu', 'plural1' => '', 'plural2' => ''],
            ['original' => 'grid.feedback.overview.action.unarchive', 'hash' => '5f1ba96c5c26946342d306f8c51a9412', 'module' => 'admin', 'language_id' => 1, 'singular' => '', 'plural1' => '', 'plural2' => ''],
            ['original' => 'grid.feedback.overview.action.unarchive.title', 'hash' => 'b0fc7c4364739793154fa06c544eaa23', 'module' => 'admin', 'language_id' => 1, 'singular' => 'přesunout z archívu', 'plural1' => '', 'plural2' => ''],
        ];

        foreach ($translations as $translation) {
            $this->addSql('DELETE FROM translation WHERE hash = :hash', $translation);
            $this->addSql('SELECT create_translation(:original, :hash, :module, :language_id, :singular, :plural1, :plural2)', $translation);
        }
    }

    public function down(Schema $schema) : void
    {
        // this down() migration is auto-generated, please modify it to your needs
    }
}
