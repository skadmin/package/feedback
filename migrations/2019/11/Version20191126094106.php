<?php

declare(strict_types=1);

namespace Migrations;

use Doctrine\DBAL\Schema\Schema;
use Doctrine\Migrations\AbstractMigration;
use Nette\Utils\DateTime;
use Skadmin\Feedback\BaseControl;
use Skadmin\Feedback\Mail\CMailFeedbackCreate;
use function serialize;

/**
 * Auto-generated Migration: Please modify to your needs!
 */
final class Version20191126094106 extends AbstractMigration
{
    public function getDescription() : string
    {
        return '';
    }

    public function up(Schema $schema) : void
    {
        $data = [
            'base_control' => BaseControl::class,
            'webalize'     => 'feedback',
            'name'         => 'Feedback',
            'is_active'    => 1,
        ];

        $this->addSql(
            'INSERT INTO core_package (base_control, webalize, name, is_active) VALUES (:base_control, :webalize, :name, :is_active)',
            $data
        );

        $resources = [
            [
                'name'        => 'feedback',
                'title'       => 'role-resource.feedback.title',
                'description' => 'role-resource.feedback.description',
            ],
        ];

        foreach ($resources as $resource) {
            $this->addSql('INSERT INTO core_role_resource (name, title, description) VALUES (:name, :title, :description)', $resource);
        }

        $mailTemplates = [
            [
                'content'            => '',
                'parameters'         => serialize(CMailFeedbackCreate::getModelForSerialize()),
                'type'               => CMailFeedbackCreate::TYPE,
                'class'              => CMailFeedbackCreate::class,
                'name'               => 'mail.feedback-create.name',
                'subject'            => 'mail.feedback-create.subject',
                'last_update_author' => 'Cron',
                'last_update_at'     => new DateTime(),
            ],
        ];

        foreach ($mailTemplates as $mailTemplate) {
            $this->addSql('INSERT INTO mail_template (content, parameters, type, class, name, subject, last_update_author, last_update_at) VALUES (:content, :parameters, :type, :class, :name, :subject, :last_update_author, :last_update_at)', $mailTemplate);
        }
    }

    public function down(Schema $schema) : void
    {
    }
}
