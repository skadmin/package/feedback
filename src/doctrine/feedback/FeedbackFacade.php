<?php

declare(strict_types=1);

namespace Skadmin\Feedback\Doctrine\Feedback;

use SkadminUtils\DoctrineTraits\Facade;
use Nette\Utils\Random;
use Nette\Utils\Strings;
use Nettrine\ORM\EntityManagerDecorator;

final class FeedbackFacade extends Facade
{
    public function __construct(EntityManagerDecorator $em)
    {
        parent::__construct($em);
        $this->table = Feedback::class;
    }

    public function create(string $name, string $content, string $userName, string $userEmail) : Feedback
    {
        $code          = Strings::upper(Random::generate());
        $existWithCode = $this->existWithCode($code);

        while ($existWithCode) {
            $code          = Strings::upper(Random::generate());
            $existWithCode = $this->existWithCode($code);
        }

        return $this->update(null, $name, $content, $userName, $userEmail, $code);
    }

    public function existWithCode(string $code) : bool
    {
        $criteria = ['code' => $code];

        $feedback = $this->em
            ->getRepository($this->table)
            ->findOneBy($criteria);

        return $feedback !== null;
    }

    public function update(?int $id, string $name, string $content, string $userName, string $userEmail, ?string $code = null) : Feedback
    {
        $feedback = $this->get($id);

        $feedback->update($name, $content, $userName, $userEmail, $code);

        $this->em->persist($feedback);
        $this->em->flush();

        return $feedback;
    }

    public function get(?int $id = null) : Feedback
    {
        if ($id === null) {
            return new Feedback();
        }

        $feedback = parent::get($id);

        if ($feedback === null) {
            return new Feedback();
        }

        return $feedback;
    }

    public function archive(Feedback $feedback) : Feedback
    {
        $feedback->archive();

        $this->em->persist($feedback);
        $this->em->flush();

        return $feedback;
    }

    public function unarchive(Feedback $feedback) : Feedback
    {
        $feedback->unarchive();

        $this->em->persist($feedback);
        $this->em->flush();

        return $feedback;
    }
}
