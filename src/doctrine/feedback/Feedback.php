<?php

declare(strict_types=1);

namespace Skadmin\Feedback\Doctrine\Feedback;

use SkadminUtils\DoctrineTraits\Entity;
use Doctrine\ORM\Mapping as ORM;

#[ORM\Entity]
#[ORM\HasLifecycleCallbacks]
class Feedback
{
    use Entity\Id;
    use Entity\Content;
    use Entity\Name;
    use Entity\StaticUserData;
    use Entity\Created;
    use Entity\Code;
    use Entity\IsActive;

    public function __construct()
    {
        $this->isActive = true;
    }

    public function update(string $name, string $content, string $userName, string $userEmail, ?string $code = null): void
    {
        $this->name        = $name;
        $this->content     = $content;
        $this->staticName  = $userName;
        $this->staticEmail = $userEmail;

        if ($code === null) {
            return;
        }

        $this->code = $code;
    }

    public function archive(): void
    {
        $this->isActive = false;
    }

    public function unarchive(): void
    {
        $this->isActive = true;
    }
}
