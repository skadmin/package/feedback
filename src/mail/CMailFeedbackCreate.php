<?php

declare(strict_types=1);

namespace Skadmin\Feedback\Mail;

use Skadmin\Mailing\Model\CMail;
use Skadmin\Mailing\Model\MailParameterValue;
use Skadmin\Mailing\Model\MailTemplateParameter;
use DateTimeInterface;
use function sprintf;

class CMailFeedbackCreate extends CMail
{
    public const TYPE = 'feedback-create';

    /** @var string */
    private $name;

    /** @var string */
    private $content;

    /** @var string */
    private $userName;

    /** @var string */
    private $userEmail;

    /** @var string */
    private $code;

    /** @var DateTimeInterface */
    private $createdAt;

    public function __construct(string $name, string $content, string $userName, string $userEmail, string $code, DateTimeInterface $createdAt)
    {
        $this->name      = $name;
        $this->content   = $content;
        $this->userName  = $userName;
        $this->userEmail = $userEmail;
        $this->code      = $code;
        $this->createdAt = $createdAt;
    }

    /**
     * @return mixed[]
     */
    public static function getModelForSerialize() : array
    {
        $params = ['name', 'content', 'user-name', 'user-email', 'code', 'created-at'];
        $model  = [];

        foreach ($params as $param) {
            $description = sprintf('mail.feedback-create.parameter.%s.description', $param);
            $example     = sprintf('mail.feedback-create.parameter.%s.example', $param);

            $model[] = (new MailTemplateParameter($param, $description, $example))->getDataForSerialize();
        }

        return $model;
    }

    /**
     * @return MailParameterValue[]
     */
    public function getParameterValues() : array
    {
        return [
            new MailParameterValue('name', $this->getName()),
            new MailParameterValue('content', $this->getContent()),
            new MailParameterValue('user-name', $this->getUserName()),
            new MailParameterValue('user-email', $this->getUserEmail()),
            new MailParameterValue('code', $this->getCode()),
            new MailParameterValue('created-at', $this->getCreatedAt()),
        ];
    }

    public function getName() : string
    {
        return $this->name;
    }

    public function setName(string $name) : self
    {
        $this->name = $name;
        return $this;
    }

    public function getContent() : string
    {
        return $this->content;
    }

    public function setContent(string $content) : self
    {
        $this->content = $content;
        return $this;
    }

    public function getUserName() : string
    {
        return $this->userName;
    }

    public function setUserName(string $userName) : self
    {
        $this->userName = $userName;
        return $this;
    }

    public function getUserEmail() : string
    {
        return $this->userEmail;
    }

    public function setUserEmail(string $userEmail) : self
    {
        $this->userEmail = $userEmail;
        return $this;
    }

    public function getCode() : string
    {
        return $this->code;
    }

    public function setCode(string $code) : self
    {
        $this->code = $code;
        return $this;
    }

    public function getCreatedAt() : DateTimeInterface
    {
        return $this->createdAt;
    }

    public function setCreatedAt(DateTimeInterface $createdAt) : self
    {
        $this->createdAt = $createdAt;
        return $this;
    }
}
