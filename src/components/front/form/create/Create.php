<?php

declare(strict_types=1);

namespace Skadmin\Feedback\Components\Front;

use App\Components\Form\FormWithUserControl;
use Skadmin\Mailing\Doctrine\Mail\MailQueue;
use Skadmin\Mailing\Model\MailService;
use App\Model\System\APackageControl;
use App\Model\System\Flash;
use Nette\Security\User as LoggedUser;
use Nette\Utils\ArrayHash;
use Skadmin\Feedback\Doctrine\Feedback\FeedbackFacade;
use Skadmin\Feedback\Mail\CMailFeedbackCreate;
use Skadmin\Translator\SimpleTranslation;
use Skadmin\Translator\Translator;
use SkadminUtils\FormControls\UI\Form;

/**
 * Class Edit
 */
class Create extends FormWithUserControl
{
    use APackageControl;

    /** @var FeedbackFacade */
    private $facade;

    /** @var MailService */
    private $mailService;

    public function __construct(FeedbackFacade $facade, Translator $translator, MailService $mailService, LoggedUser $user)
    {
        parent::__construct($translator, $user);
        $this->facade      = $facade;
        $this->mailService = $mailService;
    }

    public function getTitle() : string
    {
        return 'form.feedback.front.create.title';
    }

    public function processOnSuccess(Form $form, ArrayHash $values) : void
    {
        $feedback = $this->facade->create($values->name, $values->content, $this->loggedUser->getIdentity()->getFullName(), $this->loggedUser->getIdentity()->getEmail());

        $cMailFeedbackCreate = new CMailFeedbackCreate(
            $feedback->getName(),
            $feedback->getContent(),
            $feedback->getStaticName(),
            $feedback->getStaticEmail(),
            $feedback->getCode(),
            $feedback->getCreatedAt()
        );

        $mailQueue = $this->mailService->addByTemplateType(
            CMailFeedbackCreate::TYPE,
            $cMailFeedbackCreate,
            [$this->loggedUser->getIdentity()->getEmail()],
            true
        );

        if ($mailQueue !== null && $mailQueue->getStatus() === MailQueue::STATUS_SENT) {
            $message = new SimpleTranslation('form.feedback.front.create.flash.success-mail %s', $this->loggedUser->getIdentity()->getEmail());
            $this->onFlashmessage($message, Flash::SUCCESS);
        } else {
            $message = new SimpleTranslation('form.feedback.front.create.flash.danger %s', $this->loggedUser->getIdentity()->getEmail());
            $this->onFlashmessage($message, Flash::DANGER);
        }

        $form->reset();
        $this->redrawControl('snipForm');
    }

    public function render() : void
    {
        $template = $this->getComponentTemplate();
        $template->setTranslator($this->translator);
        $template->setFile(__DIR__ . '/create.latte');

        $template->render();
    }

    protected function createComponentForm() : Form
    {
        $form = new Form();
        $form->setTranslator($this->translator);

        $form->addText('name', 'form.feedback.front.create.name')
            ->setRequired('form.feedback.front.create.name.req');
        $form->addTextArea('content', 'form.feedback.front.create.content', null, 10)
            ->setRequired('form.feedback.front.create.content.req');

        // BUTTON
        $form->addSubmit('send', 'form.feedback.front.create.send');

        // CALLBACK
        $form->onSuccess[] = [$this, 'processOnSuccess'];

        return $form;
    }
}
