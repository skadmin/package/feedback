<?php

declare(strict_types=1);

namespace Skadmin\Feedback\Components\Admin;

use SkadminUtils\GridControls\UI\GridControl;
use SkadminUtils\GridControls\UI\GridDoctrine;
use Skadmin\Role\Doctrine\Role\Privilege;
use App\Model\System\APackageControl;
use App\Model\System\Constant;
use App\Model\System\Utils;
use Nette\ComponentModel\IContainer;
use Nette\Security\User;
use Nette\Utils\Arrays;
use Nette\Utils\Html;
use Skadmin\Feedback\BaseControl;
use Skadmin\Feedback\Doctrine\Feedback\Feedback;
use Skadmin\Feedback\Doctrine\Feedback\FeedbackFacade;
use Skadmin\Translator\Translator;
use function intval;

/**
 * Class Overview
 */
class Overview extends GridControl
{
    use APackageControl;

    public const ARCHIVE   = 0;
    public const UNARCHIVE = 1;

    public const STATUS_ARCHIVE = [
        self::UNARCHIVE => 'grid.feedback.overview.status.unarchive',
        self::ARCHIVE   => 'grid.feedback.overview.status.archive',
    ];

    /** @var FeedbackFacade */
    private $facade;

    public function __construct(FeedbackFacade $facade, Translator $translator, User $user)
    {
        parent::__construct($translator, $user);

        $this->facade = $facade;
    }

    /**
     * @return static
     */
    public function setParent(?IContainer $parent, ?string $name = null)
    {
        parent::setParent($parent, $name);

        if (! $this->isAllowed(BaseControl::RESOURCE, Privilege::READ)) {
            $this->getParent()->redirect(':Admin:Homepage:accessDenied');
        }

        return $this;
    }

    public function render() : void
    {
        $template = $this->getComponentTemplate();
        $template->setTranslator($this->translator);
        $template->setFile(__DIR__ . '/overview.latte');
        $template->render();
    }

    public function getTitle() : string
    {
        return 'feedback.overview.title';
    }

    protected function createComponentGrid(string $name) : GridDoctrine
    {
        $grid = new GridDoctrine($this->getPresenter());

        // DEFAULT
        $grid->setPrimaryKey('id');
        $grid->setDataSource($this->facade->getModel());

        // DATA
        $dataArchive = Arrays::map(self::STATUS_ARCHIVE, function ($item) : string {
            return $this->translator->translate($item);
        });

        // COLUMNS
        $grid->addColumnText('name', 'grid.feedback.overview.name')
            ->setRenderer(static function (Feedback $feedback) : Html {
                $name = Html::el('span', ['class' => 'text-primary font-weight-bold'])->setText($feedback->getName());

                $code = Html::el('code', ['class' => 'text-muted small'])->setText($feedback->getCode());

                $sender = new Html();
                $sender->addHtml($name)
                    ->addHtml('<br/>')
                    ->addHtml($code);

                return $sender;
            });
        $grid->addColumnText('sender', 'grid.feedback.overview.static-name')
            ->setRenderer(static function (Feedback $feedback) : Html {
                $sender = new Html();
                $sender->addText($feedback->getStaticName())
                    ->addHtml('<br/>')
                    ->addHtml(Utils::createHtmlContact($feedback->getStaticEmail()));

                return $sender;
            });
        $grid->addColumnDateTime('createdAt', 'grid.feedback.overview.created-at')
            ->setFormat('d.m.Y H:i')
            ->setSortable();
        $grid->addColumnText('isActive', 'grid.feedback.overview.is-active')
            ->setAlign('center')
            ->setRenderer(static function (Feedback $feedback) use ($dataArchive) : Html {
                $status = Html::el('span');

                if ($feedback->isActive()) {
                    $status->setText($dataArchive[self::UNARCHIVE]);
                } else {
                    $status->setText($dataArchive[self::ARCHIVE])
                        ->addAttributes(['class' => 'text-danger']);
                }

                return $status;
            });

        // FILTER
        $grid->addFilterText('name', 'grid.feedback.overview.name', ['name', 'code']);
        $grid->addFilterText('sender', 'grid.feedback.overview.static-name', ['staticName', 'staticEmail']);
        $grid->addFilterSelect('isActive', 'grid.feedback.overview.is-active', $dataArchive)
            ->setPrompt(Constant::PROMTP);

        // ACTION
        $grid->addActionCallback('archive', 'grid.feedback.overview.action.archive')
            ->setIcon('folder')
            ->setTitle('grid.feedback.overview.action.archive.title')
            ->setClass('btn btn-xs btn-outline-danger ajax')
            ->onClick[] = function (string $feedbackId) use ($grid) : void {
                $feedback = $this->facade->get(intval($feedbackId));
                $this->facade->archive($feedback);

                $grid->redrawItem($feedbackId);
            };

        $grid->addActionCallback('unarchive', 'grid.feedback.overview.action.unarchive')
            ->setIcon('folder-open')
            ->setTitle('grid.feedback.overview.action.unarchive.title')
            ->setClass('btn btn-xs btn-outline-primary ajax')
            ->onClick[] = function (string $feedbackId) use ($grid) : void {
                $feedback = $this->facade->get(intval($feedbackId));
                $this->facade->unarchive($feedback);

                $grid->redrawItem($feedbackId);
            };

        // ALLOWED ACTION
        $grid->allowRowsAction('archive', static function (Feedback $feedback) : bool {
            return $feedback->isActive();
        });

        $grid->allowRowsAction('unarchive', static function (Feedback $feedback) : bool {
            return ! $feedback->isActive();
        });

        // DETAIL
        $grid->setItemsDetail(__DIR__ . '/detail.latte');

        // OTHER
        $grid->setDefaultSort(['createdAt' => 'DESC']);
        $grid->setDefaultFilter([
            'isActive' => self::UNARCHIVE,
        ]);

        return $grid;
    }
}
